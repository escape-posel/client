import React from 'react';
import Image from 'next/image';
import HistoryTimeline from '@/screens/HomePage/components/historyTimeline/historyTimeline';
import styles from './HomePage.module.scss';

const HomePage = () => {
  return (
    <div className={styles.container}>
      <div className={styles.description}>
        Добро пожаловать на официальный сайт одной из самых перспективных команд
        в{' '}
        <Image
          className={styles.civLogo}
          src="/CivLogo.png"
          alt=""
          height={24}
          width={160}
          priority
        />
      </div>
      <div className={styles.bottomText}>
        история которой пишется прямо сейчас..
      </div>
      <HistoryTimeline />
    </div>
  );
};

export default HomePage;
