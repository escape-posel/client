'use client';
import React, {
  FC,
  forwardRef,
  SetStateAction,
  useContext,
  useRef,
} from 'react';
import Link from 'next/link';
import { SubmitHandler, useForm } from 'react-hook-form';
import { IUserLoginRequest } from '@/interfaces/user.Interface';
import UserService from '@/services/User.Service';
import { Context } from '@/context/context';
import { useOnClickOutside } from 'usehooks-ts';

interface IProps {
  setAuthorizationPanelOpened: React.Dispatch<SetStateAction<boolean>>;
}

const AuthorizationPanel = forwardRef<HTMLDivElement, IProps>((props, ref) => {
  const { setAuthorizationPanelOpened } = props;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IUserLoginRequest>();

  const { setUserAuth, setUser, setError, setSuccessMessage } =
    useContext(Context);

  const onSubmit: SubmitHandler<IUserLoginRequest> = async data => {
    try {
      const response = await UserService.login(data.email, data.password).catch(
        function (error) {
          if (error.response) {
            setError(error.response.data.message);
            throw new Error();
          } else if (error.request) {
            throw new Error();
          } else {
            console.log('Error', error.message);
            throw new Error();
          }
        },
      );
      localStorage.setItem('token', response.data.accessToken);
      setUserAuth(true);
      setSuccessMessage('Успешная авторизация!');
      setUser(response.data.user);
    } catch (e) {
      console.log(e);
    } finally {
      setAuthorizationPanelOpened(false);
    }
  };

  return (
    <div
      className={
        'absolute  border-2 bg-black/50 border-white p-3 rounded-md right-[calc(100%+0.75rem)] translate-x-1/4 top-10'
      }
      ref={ref}
    >
      <form
        onSubmit={handleSubmit(onSubmit)}
        className={'flex flex-col items-center'}
      >
        <label htmlFor="">
          Ваша почта
          <input
            {...register('email', { required: true })}
            className={'w-full px-2 rounded-md text-black'}
            type="email"
          />
        </label>
        <label htmlFor="">
          Пароль
          <input
            {...register('password', { required: true })}
            className={'w-full px-2 rounded-md text-black'}
            type="password"
          />
        </label>
        <button className={'rounded-md bg-amber-600 w-36 border-2 mt-2'}>
          Войти
        </button>
      </form>
      <div className={'text-center'}>
        Нет аккаунта?
        <Link
          className={'text-orange-500'}
          href={'/registration'}
          onClick={() => {
            setAuthorizationPanelOpened(false);
          }}
        >
          Зарегистрируйтесь!
        </Link>
      </div>
    </div>
  );
});

export default AuthorizationPanel;
