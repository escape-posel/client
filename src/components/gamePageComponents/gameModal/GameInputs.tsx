import { SubmitHandler, useFieldArray, useForm } from 'react-hook-form';
import React, { FC } from 'react';
import { ModalGameProps } from '@/components/gamePageComponents/gameModal/GameModal';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import PlayerService from '@/services/Player.Service';
import CivilizationService from '@/services/Civilization.Service';
import TeamService from '@/services/Team.Service';
import { ITeamResponse } from '@/interfaces/teams.Interface';
import { IPlayerResponse } from '@/interfaces/players.Interface';
import { ICivilizationResponse } from '@/interfaces/civilization.Interface';
import {
  IGameInput,
  IGameRequest,
  IOnePlayerInGame,
} from '@/interfaces/game.Interface';
import GameService from '@/services/Game.Service';
import TournamentService from '@/services/Tournament.Service';
import { ITournamentResponse } from '@/interfaces/tournament.interface';
import MapService from '@/services/Map.Service';
import { IGetAllMaps } from '@/interfaces/map.interface';
import { toast } from 'react-toastify';
import { useGetCreateGameInfo } from '@/components/gamePageComponents/hook/useGetCreateGameInfo';

const player = {
  playerId: 0,
  civilizationId: 0,
};

const formatDate = (date: Date) => {
  const year: number | string = date.getFullYear();
  let month: number | string = date.getMonth() + 1;
  let day: number | string = date.getDate();

  month = month < 10 ? `0${month}` : month;
  day = day < 10 ? `0${day}` : day;

  return `${year}-${month}-${day}`;
};

const GameInputs: FC<ModalGameProps> = ({
  setShowModal,
  game,
  setCurrentGame,
}) => {
  const {
    watch,
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IGameInput>({
    defaultValues: {
      gamesPlayed: 1,
      gameResult: [
        {
          value: '0',
        },
      ],
      playerList: [player, player, player, player],
    },
  });

  const {
    fields: gamesResultFields,
    append: appendNewGame,
    remove: removeNewGame,
  } = useFieldArray({
    name: 'gameResult',
    control,
  });

  const { fields: playerField } = useFieldArray({
    name: 'playerList',
    control,
  });

  const [teams, maps, tournaments, players, civilizations] =
    useGetCreateGameInfo(watch('tournament_id') ?? 0);

  const onSubmit: SubmitHandler<IGameInput> = async data => {
    const dataToSend = {
      date: data.date,
      teamId: data.teamId,
      isWin: data.gameResult.map(game => game.value !== '0'),
      players: data.playerList,
      tournamentId: data.tournament_id,
      mapId: data.mapId,
    };
    if (data.playerList.filter(player => player.playerId !== 0).length < 4) {
      toast.error('Мало игроков');
      return;
    }
    if (game) {
      // dataToSend.id = game.id;
      updateGame({
        ...dataToSend,
        isWin: dataToSend.isWin[0],
      });
      //TODO: Реализовать функцию обновления игры
      // updateGame(dataToSend)
      return;
    }
    if (dataToSend.isWin.length > 1) {
      await GameService.createGameSeries(dataToSend);
      return;
    }
    console.log(dataToSend);
    createGame({
      ...dataToSend,
      isWin: dataToSend.isWin[0],
    });
  };

  const client = useQueryClient();

  const { mutate: createGame } = useMutation(
    ['games'],
    GameService.createGame,
    {
      onSuccess: () =>
        client
          .invalidateQueries({ queryKey: ['games'] })
          .then(() => setShowModal(false)),
    },
  );
  const { mutate: updateGame } = useMutation(
    ['games'],
    GameService.updateGame,
    {
      onSuccess: () =>
        client
          .invalidateQueries({ queryKey: ['games'] })
          .then(() => setShowModal(false)),
    },
  );

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className={'flex flex-col items-center'}
    >
      <label className={'flex flex-col items-center'}>
        Дата
        <input
          className={'border border-black p-2 rounded-2xl'}
          defaultValue={game?.date ? game.date : formatDate(new Date())}
          type="date"
          {...register('date', { required: true })}
        />
      </label>
      <label className={'flex flex-col items-center'}>
        Турнир
        <select
          className={'border border-black p-2 rounded-2xl'}
          defaultValue={game?.TournamentId ? game.TournamentId : ''}
          {...register('tournament_id', { required: true })}
        >
          {tournaments.data &&
            tournaments.data.map(tournament => (
              <option
                key={`tournament_option_${tournament.id}`}
                value={tournament.id}
              >
                {tournament.name}
              </option>
            ))}
        </select>
      </label>
      <label className={'flex flex-col items-center'}>
        Соперник
        <select
          className={'border border-black p-2 rounded-2xl'}
          defaultValue={game?.TeamId ? game.TeamId : ''}
          {...register('teamId', { required: true })}
        >
          {teams.data &&
            teams.data.map(team => (
              <option key={`team_option_${team.id}`} value={team.id}>
                {team.name}
              </option>
            ))}
        </select>
      </label>
      <label className={'flex flex-col items-center'}>
        Карта
        <select
          className={'border border-black p-2 rounded-2xl'}
          defaultValue={game?.MapId ? game.MapId : ''}
          {...register('mapId', { required: true })}
        >
          {maps.data &&
            maps.data.map(map => (
              <option key={`map_option_${map.id}`} value={map.id}>
                {map.name}
              </option>
            ))}
        </select>
      </label>
      {gamesResultFields.map((field, index) => (
        <>
          <div className={'flex flex-col items-center'}>
            Результат игры {index + 1}
          </div>
          <div className={'flex gap-x-4'}>
            <label>
              Победа
              <input
                key={field.id} // important to include key with field's id
                {...register(`gameResult.${index}`)}
                className={'ml-2'}
                type="radio"
                value={1}
                checked={game?.isWin}
              />
            </label>
            <label>
              Поражение
              <input
                className={'ml-2'}
                type="radio"
                value={0}
                checked={!game?.isWin}
                key={field.id} // important to include key with field's id
                {...register(`gameResult.${index}`)}
              />
            </label>
          </div>
        </>
      ))}
      <div className={'flex gap-x-2'}>
        <button
          type={'button'}
          className={'bg-blue-400 p-2 text-white rounded-2xl'}
          onClick={() => removeNewGame()}
        >
          Удалить игру
        </button>
        <button
          type={'button'}
          className={'bg-blue-400 p-2 text-white rounded-2xl'}
          onClick={() =>
            appendNewGame({
              value: '0',
            })
          }
        >
          Добавить игру
        </button>
      </div>
      {!players.isFetching &&
        !civilizations.isFetching &&
        players.data &&
        civilizations.data &&
        players.data.length > 0 &&
        civilizations.data.length > 0 && (
          <div>
            {playerField.map((field, index) => {
              return (
                <div className={'flex flex-col lg:flex-row'} key={field.id}>
                  <label className={'flex flex-col items-center'}>
                    Игрок {index + 1}
                    <select
                      className={'border border-black p-2 rounded-2xl'}
                      defaultValue={
                        game?.players[0] ? game?.players[0].player.id : ''
                      }
                      {...register(`playerList.${index}.playerId`)}
                    >
                      {players.data.map(player => (
                        <option
                          key={`player1_option_${player.id}`}
                          value={player.id}
                        >
                          {player.nickname}
                        </option>
                      ))}
                    </select>
                  </label>
                  <label className={'flex flex-col items-center'}>
                    Цивилизация {index + 1}
                    <select
                      className={'border border-black p-2 rounded-2xl'}
                      defaultValue={
                        game?.players[0] ? game?.players[0].civilization.id : ''
                      }
                      {...register(`playerList.${index}.civilizationId`)}
                    >
                      {civilizations.data.map(civilization => (
                        <option
                          key={`civilization1_option_${civilization.id}`}
                          value={civilization.id}
                        >
                          {civilization.name}
                        </option>
                      ))}
                    </select>
                  </label>
                </div>
              );
            })}
          </div>
        )}
      <input
        className={'bg-green-300 p-2 mt-5 rounded-xl cursor-pointer'}
        type="submit"
      />
    </form>
  );
};

export default GameInputs;
