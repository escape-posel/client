'use client';
import React, {
  FC,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import clsx from 'clsx';

interface IGameOptionsProps {
  withActiveFilter?: boolean;
  onlyActivePlayer?: boolean;
  setOnlyActivePlayer?: React.Dispatch<SetStateAction<boolean>>;
}

const GameOptions: FC<IGameOptionsProps> = ({
  withActiveFilter = false,
  onlyActivePlayer,
  setOnlyActivePlayer,
}) => {
  const pathname = usePathname();
  const router = useRouter();
  const searchParams = useSearchParams()!;

  const isOnlyOfficialGame =
    useSearchParams().get('onlyOfficialGame') === 'true';
  const [isButtonActive, setButtonActive] = useState(isOnlyOfficialGame);

  useEffect(() => {
    setButtonActive(isOnlyOfficialGame);
  }, [pathname, searchParams]);

  useEffect(() => {
    if (isButtonActive)
      router.replace(
        pathname + '?' + createQueryString('onlyOfficialGame', 'true'),
        { scroll: false },
      );
    else
      router.replace(
        pathname + '?' + createQueryString('onlyOfficialGame', 'false'),
        { scroll: false },
      );
  }, [isButtonActive]);

  const createQueryString = useCallback(
    (name: string, value: string) => {
      // @ts-ignore
      const params = new URLSearchParams(searchParams);
      params.set(name, value);
      return params.toString();
    },
    [searchParams],
  );

  const handleClick = () => {
    setButtonActive(!isButtonActive);
  };

  return (
    <div className={'flex gap-x-2'}>
      <div
        className={clsx('border p-2 rounded-2xl cursor-pointer', {
          ['bg-gradient-to-br from-[#6DA5C0] to-[#294D61]']: isButtonActive,
          ['hover:bg-gradient-to-br from-[#0F969c] to-[#6DA5C0]']:
            !isButtonActive,
        })}
        onClick={handleClick}
      >
        Только турнирные игры
      </div>
      {withActiveFilter && setOnlyActivePlayer && (
        <div
          className={clsx('border p-2 rounded-2xl cursor-pointer', {
            ['bg-gradient-to-br from-[#6DA5C0] to-[#294D61]']: onlyActivePlayer,
            ['hover:bg-gradient-to-br from-[#0F969c] to-[#6DA5C0]']:
              !onlyActivePlayer,
          })}
          onClick={() => setOnlyActivePlayer(prev => !prev)}
        >
          Только действующие игроки
        </div>
      )}
    </div>
  );
};

export default GameOptions;
