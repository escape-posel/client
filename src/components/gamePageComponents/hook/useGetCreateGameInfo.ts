import {useQueries, useQuery} from '@tanstack/react-query';
import {ITeamResponse} from '@/interfaces/teams.Interface';
import TeamService from '@/services/Team.Service';
import {IGetAllMaps} from '@/interfaces/map.interface';
import MapService from '@/services/Map.Service';
import {ITournamentResponse} from '@/interfaces/tournament.interface';
import TournamentService from '@/services/Tournament.Service';
import {IPlayerResponse} from '@/interfaces/players.Interface';
import PlayerService from '@/services/Player.Service';
import {ICivilizationResponse} from '@/interfaces/civilization.Interface';
import CivilizationService from '@/services/Civilization.Service';

const createTeamQueryKey = (tournamentId?: number) =>
    (!tournamentId || tournamentId === 2) ? ['teams'] : ['teams', `tournament_${tournamentId}`]

export const useGetCreateGameInfo = (tournamentId: number) =>
    useQueries({
        queries: [
            {
                queryKey: createTeamQueryKey(tournamentId),
                queryFn: () => TeamService.getTeams(tournamentId === 0 ? undefined : tournamentId),
            },
            {
                queryKey: ['maps'],
                queryFn: MapService.getAllMaps,
            },
            {
                queryKey: ['tournaments'],
                queryFn: TournamentService.getTournaments,
            },
            {
                queryKey: ['players'],
                queryFn: PlayerService.getActivePlayers,
            },
            {
                queryKey: ['civilizations'],
                queryFn: CivilizationService.getCivilizations,
            },
        ],
    });
