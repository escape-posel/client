import React from 'react';
import Link from 'next/link';
import PersonalCabinetButton from '@/components/layoutComponents/header/components/PersonalCabinetButton/PersonalCabinetButton';
import NavigationsButton from '@/components/layoutComponents/header/components/NavigationsButton/NavigationsButton';
import HamburgerButton from '@/components/hamburgerMenu/HamburgerButton';
import { SideMenu } from '@/components/hamburgerMenu/SideBar';
import styles from './Header.module.scss';
import Image from 'next/image';

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.content}>
        <Link href={'/'} className={styles.logoWrapper}>
          <div className={styles.mainTitle}>Escape Posel</div>
          {/*<div className={styles.additionTitle}>junior</div>*/}
          <Image
            className={'block'}
            src="/logo.webp"
            alt=""
            height={48}
            width={48}
          />
        </Link>
        <HamburgerButton />
        <div className={styles.navLinks}>
          <NavigationsButton />
          <PersonalCabinetButton />
          {/*TODO: Сделать функцию logout*/}
        </div>
      </div>
      <SideMenu />
    </header>
  );
};

export default Header;
