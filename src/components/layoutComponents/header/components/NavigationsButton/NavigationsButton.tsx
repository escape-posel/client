'use client';
import React from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import styles from '../HeaderComponents.module.scss';
import clsx from 'clsx';

const links = [
  {
    title: 'Главная',
    href: '',
  },
  {
    title: 'Состав',
    href: 'squad',
  },
  {
    title: 'Результаты',
    href: 'results',
  },
  {
    title: 'История игр',
    href: 'games',
  },
  {
    title: 'Статистика',
    href: 'statistics',
  },
];

const NavigationsButton = () => {
  const pathName = usePathname().split('/');
  return links.map(link => (
    <Link
      href={'/' + link.href}
      className={clsx(styles.navItem, {
        ['text-[#6DA5C0]']: pathName[1] === link.href,
      })}
    >
      {link.title}
    </Link>
  ));
};

export default NavigationsButton;
