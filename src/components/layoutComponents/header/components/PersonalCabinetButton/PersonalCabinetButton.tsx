'use client';
import React, { useContext, useRef, useState } from 'react';
import AuthorizationPanel from '@/components/authorizationPanel/AuthorizationPanel';
import { Context } from '@/context/context';
import Link from 'next/link';
import { useOnClickOutside } from 'usehooks-ts';
import { usePathname } from 'next/navigation';
import sharedStyles from '../HeaderComponents.module.scss';
import clsx from 'clsx';

const PersonalCabinetButton = () => {
  const [isAuthorizationPanelOpened, setAuthorizationPanelOpened] =
    useState(false);
  const { isUserAuth, isLoading } = useContext(Context);
  const authorizationPanelRef = useRef(null);
  const pathName = usePathname().split('/');

  const handleClickOutside = () => {
    setAuthorizationPanelOpened(false);
  };

  useOnClickOutside(authorizationPanelRef, handleClickOutside);
  const toggleAuthorizationPanel = () => {
    setAuthorizationPanelOpened(!isAuthorizationPanelOpened);
  };

  if (isLoading)
    return (
      <div className={'flex justify-center h-fit relative'}>Загрузка...</div>
    );

  if (isUserAuth)
    return (
      <div className={'relative'}>
        <Link
          href={'/profile'}
          className={clsx(sharedStyles.navItem, {
            ['text-[#6DA5C0]']: pathName[1] === 'profile',
          })}
        >
          Личный кабинет
        </Link>
      </div>
    );

  return (
    <div className={'relative'}>
      <div onClick={toggleAuthorizationPanel} className={sharedStyles.navItem}>
        Вход
      </div>
      {isAuthorizationPanelOpened && (
        <AuthorizationPanel
          setAuthorizationPanelOpened={setAuthorizationPanelOpened}
          ref={authorizationPanelRef}
        />
      )}
    </div>
  );
};

export default PersonalCabinetButton;
