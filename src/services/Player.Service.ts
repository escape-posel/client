import {
  IPlayerRequest,
  IPlayerResponse,
} from '@/interfaces/players.Interface';
import $api, { API_URL, PUBLIC_API_URL } from '@/http';
import axios from 'axios';

class PlayerService {
  async getPlayers(): Promise<IPlayerResponse[]> {
    return await fetch(`${PUBLIC_API_URL}/api/player/get-all-players`, {
      next: {
        revalidate: 10,
      },
    }).then(res => res.json());
  }

  async getActivePlayers(): Promise<IPlayerResponse[]> {
    const response = await axios.get(
      `${PUBLIC_API_URL}/api/player/get-all-players`,
      {
        params: {
          active: true,
        },
      },
    );
    return response.data;
  }

  async getPlayerInfo(id: number, isOnlyOfficialGame: boolean) {
    const response = await fetch(
      `${API_URL}/api/player/get-player-info/${id}?officialGameOnly=${isOnlyOfficialGame}`,
      {
        next: {
          revalidate: 10,
        },
      },
    );
    return response.json();
  }

  async createPlayer(data: IPlayerRequest) {
    return await $api.post(`${PUBLIC_API_URL}/api/player/create`, data);
  }

  async updatePlayer(data: IPlayerRequest) {
    return await $api.put(`${PUBLIC_API_URL}/api/player/update`, data);
  }

  async deletePlayer(id: number) {
    return await $api.delete(`${PUBLIC_API_URL}/api/player/delete`, {
      data: { id },
    });
  }
}

export default new PlayerService();
