export const convertDateFunc = (date: Date) => {
  const day = date.getUTCDate();
  const month =
    date.getUTCMonth() + 1 < 10
      ? '0' + Number(date.getUTCMonth() + 1)
      : date.getUTCMonth() + 1;
  const year = date.getUTCFullYear();
  return day + '.' + month + '.' + year;
};
