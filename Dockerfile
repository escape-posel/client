FROM node:21-alpine as builder

WORKDIR /usr/src/client
COPY ./ /usr/src/client

COPY ["./package.json","./package-lock.json*","./"]

RUN npm install
RUN npm install sharp

COPY . .

RUN npm run build

CMD ["npm","run","start"]
